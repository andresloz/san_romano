#! /usr/bin/env python3
#-*- coding: utf-8 -*-

from adafruit_servokit import ServoKit
from time import sleep
from random import shuffle, choice
import opc

NAME = "San Romano Battle"
VERSION = "0.0.1"
DATE = "2020 January 12"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://hyperficiel.com"
DESCRIPTION = "La défaite du camp siennois illustrée par la mise hors de combat de Bernardino della Ciarda, (~1456) détrempe sur bois de 3,23 × 1,80 m (Galerie des Offices, Florence), seul panneau signé"

dutyTime = 0.02

# servos
kit = ServoKit(channels=16)
# 0-7 left servos
# 8-15 right servos
servos = list(range(16))
servoAngle = []
for ser in servos:
	servoAngle.append([ser,None])

stepsLeft = list(range(0,70,22))
stepsRight = list(range(70,180,22))

serSteps = [
	# 0-7 left servos 
	list(range(0,40,10)), # 0 yellow
	list(range(10,40,10)), # 1 black 
	stepsLeft, # 2
	stepsLeft, # 3
	stepsLeft, # 4
	stepsLeft, # 5
	stepsLeft, # 6
	stepsLeft, # 7
	# 8-15 right servos
	list(range(140,180,10)), # 8 broked
	stepsRight, # 9
	stepsRight, # 10
	stepsRight, # 11
	stepsRight, # 12
	stepsRight, # 13
	stepsRight, # 14
	stepsRight  # 15
]

# leds
neo = opc.Client('localhost:7890')
numLEDs = 32
stripOff = [ (0,0,0) ] * numLEDs
stripLED = [ (0,0,0) ] * numLEDs

# white spot leds
stripLED[-1] = stripLED[-2] = (230,) * 3
stripOff[-1] = stripOff[-2] = (230,) * 3

flashLed = [2,3,4,8,9,11,12,15,18,19,20,21,22,23,24,25] # nearest leds to servos, only 16 leds in use
flashLed.reverse()


# r, g, b led set primary/secondary colors
ledColor = [(0,255,255), (255,0,255), (255,255,0), (255,0,0), (0,255,0), (0,0,255)]

# init
neo.put_pixels(stripOff)
sleep(dutyTime)

try:
	while True:
		shuffle(servos)
		for ser in servos:
			angle = choice(serSteps[ser])
			if servoAngle[ser] != angle: # change angle if new value
				# change servo angle
				servoAngle[ser] = angle
				kit.servo[ser].angle = angle

				# flash nearest led to servo
				stripLED[flashLed[ser]] = choice(ledColor) # choose random color
				neo.put_pixels(stripLED)
				sleep(dutyTime)
				stripLED[flashLed[ser]] = (0,0,0) # switch off led
				neo.put_pixels(stripOff) 
				sleep(dutyTime)

except KeyboardInterrupt:
	# switch off all led
	# switch off white spot leds
	stripOff[-1] = stripOff[-2] = (0,) * 3
	print("\nStop swicth off leds")
	neo.put_pixels(stripOff)
	neo.disconnect()

	# reset servos to initial position
	print("\nStop! reset servos")
	for ser in range(0,16):
		if  ser < 8:
			if servoAngle[ser] != max(serSteps[ser]):
				kit.servo[ser].angle = max(serSteps[ser])
				sleep(dutyTime)
		else:
			if servoAngle[ser] != min(serSteps[ser]):
				kit.servo[ser].angle = min(serSteps[ser])
				sleep(dutyTime)

