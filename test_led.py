#!/usr/bin/env python
"""
usage 

sudo ./fcserver-rpi &

python test_led.py
or
python test_led.py number_of_leds start_index_led duty_time
"""

from __future__ import print_function
import opc, time, sys
from random import randint, choice
startLED = 0
if len(sys.argv) > 2:startLED = int(sys.argv[2])

numLEDs = startLED + 8
if len(sys.argv) > 1:numLEDs = startLED + int(sys.argv[1])

duty = 0.1
if len(sys.argv) > 3:duty = float(sys.argv[3])

neo = opc.Client('localhost:7890')

off = [ (0,0,0) ] * numLEDs
stripe = [ (0,0,0) ] * numLEDs

colors = []
max = 230
min = 50
i = 0
while i < 256:
	colors.append((randint(min,max), randint(min,max), randint(min,max)))
	i += 1

print("start led test")
neo.put_pixels(off)
time.sleep(duty)
while True:
	color = choice(colors)
	try:
		for index in range(startLED,numLEDs):
			stripe[index] = color
			neo.put_pixels(stripe)
			time.sleep(duty)
			stripe[index] = (0,0,0)
			neo.put_pixels(off)
			time.sleep(duty)

	except KeyboardInterrupt:
		neo.put_pixels(off)
		neo.disconnect()
		print("exit")
		exit()


