# San Romano Project  

![Project image](http://hyperficiel.com/san_romano/view.jpg)  

## This script use Fadecandy, Servo Hat, RGB LED Strip and servo SG90 with raspberry pi  

[FadeCandy - Dithering USB-Controlled Driver for RGB NeoPixels](https://www.adafruit.com/product/1689)  
[Adafruit 16-Channel PWM / Servo HAT for Raspberry Pi - Mini Kit](https://www.adafruit.com/product/2327)  
[Adafruit NeoPixel Digital RGB LED Strip - White 30 LED](https://www.adafruit.com/product/1376)  
[Servo motor SG90](http://www.ee.ic.ac.uk/pcheung/teaching/DE1_EE/stores/sg90_datasheet.pdf)  

In order to use fadecandy you need to run fadecandy server first (fcserver.rpi)  
Connect 16 SG90 servo motors to servo hat  
Connect fadecandy to the led strip  

**Andres Lozano Gallego a.k.a Loz, 2019-2020**  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License http://www.artlibre.org

